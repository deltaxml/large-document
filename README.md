# Large Document Sample (DITA-OT User Guide)

*The instructions here assume that the sample resources have been checked-out, cloned or downloaded and unzipped into the samples directory of the DITA-Compare release. The resources should be located such that they are two levels below the top level release directory that contains the jar files.*

*For example `DeltaXML-DITA-Compare-8_0_0_j/samples/sample-name`.*

---

## Summary

This sample compares two versions of the DITA Open Toolkit's User Guide, doc-1-6-3/userguide.ditamap and doc-1-7-2/userguide.ditamap, using the 'topic set', 'map pair' and 'unified map' map result structures. This is intended to illustrates the results of the comparison on a reasonable sized document. For concept details see: [DITA-OT User Guide](https://docs.deltaxml.com/dita-compare/latest/large-document-sample-dita-ot-user-guide-2135657.html).

## Script Features

The output of the 'topic set', 'map pair' and 'unified map' comparisons are put into the topic-set-A, map-pair-B and unified-map-B directories respectively. These directories contain a result-alias.ditamap file whose content references the actual results, but can be used as a result in its own right. Note that in the case of the map pair result, the alias combines the two main result maps into a single document.

## Running the sample via the Ant build script

The sample comparison can be run via an Apache Ant build script using the following commands.

| Command | Actions Performed |
| --- | --- |
| ant run | Run all comparisons. |
| ant run-topic-set-A | Run the 'topic set' comparison. |
| ant run-map-pair-B | Run the 'map pair' comparison. |
| ant run-unified-map-B | Run the 'unified map' comparison. |

If you don't have Ant installed, you can still run each sample individually from the command-line.

## Running the sample from the Command line

The sample can be run directly by issuing the following command

    java -jar ../../deltaxml-dita.jar compare map doc-1-6-3/userguide.ditamap doc-1-7-2/userguide.ditamap topic-set-A map-result-origin=A map-result-structure=topic-set

The 'map pair' or 'unified map' comparisons can be run in a similar manner to the 'topic set', but in this case the map-result-origin is B, the map-result-structure is map-pair or unified-map, and the map-copy-location is map-pair-B or unified-map-B.

To clean up the sample directory, run the following Ant command.

	ant clean